# DF Studio Students

1. [repo](https://gitlab.com/timeritualslabour/digital-fabrication) [www](https://timeritualslabour.gitlab.io/digital-fabrication/) Vytautas Bikauskas
1. [repo](https://gitlab.com/l-lu-u/2024-digital-fabrication/) [www](https://l-lu-u.gitlab.io/2024-digital-fabrication/) Lu Chen
1. [repo](https://gitlab.com/grahamfeatherstone/digital-fabrication) [www](https://grahamfeatherstone.gitlab.io/digital-fabrication/) Graham Anthony Featherstone
1. [repo](https://gitlab.com/miro.keimioniemi/digital-fabrication-portfolio) [www](https://digital-fabrication-portfolio-miro-keimioniemi-a2f2c11a6e705b8f.gitlab.io/) Miro Jesper Keimiöniemi
1. [repo](https://gitlab.com/mikko.aleksis.linko/digital-fabrication) [www](https://digital-fabrication-mikko-aleksis-linko-68d09a63e975c53665cdd90.gitlab.io/) Mikko Aleksis Linko
1. [repo](https://gitlab.com/TomiMonahan/digital-fabrication/) [www](https://tomimonahan.gitlab.io/digital-fabrication/) Tomi Jari Monahan
1. [repo](https://gitlab.com/veikkoraty/digital-fabrication) [www](https://veikkoraty.gitlab.io/digital-fabrication/) Veikko Lauri Iivari Räty
1. [repo](https://github.com/teodosin/digital-fabrication) [www](https://teodosin.github.io/digital-fabrication/) Viktor Andrej Teodosin
1. [repo](https://gitlab.com/thvo-fi/test-portfolio) [www](https://0nitfans.com/) Chi Thanh Vo
1. [repo](https://gitlab.com/zruii/digital-fabrication) [www](https://zruii.gitlab.io/digital-fabrication/) Rui Zeng
