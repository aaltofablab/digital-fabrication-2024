# Digital Fabrication 2024

Here you can find list of student repositories and links to their websites.

- [Randomizer](https://aaltofablab.gitlab.io/digital-fabrication-2024/)
- [Final Project Presentations](https://aaltofablab.gitlab.io/digital-fabrication-2024/presentations.html)
- [DF I Student List](DF_I_STUDENTS.md)
- [DF II Student List](DF_II_STUDENTS.md)
- [DF Studio Student List](DF_Studio_STUDENTS.md)
