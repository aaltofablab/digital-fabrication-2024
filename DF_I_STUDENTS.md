# DF I Students

1. [repo](https://gitlab.com/AoA2810/digital-fabrication-alex/) [www](https://aoa2810.gitlab.io/digital-fabrication-alex/) Aleksandra Olegovna Artemenko
1. [repo](https://gitlab.com/timeritualslabour/digital-fabrication) [www](https://timeritualslabour.gitlab.io/digital-fabrication/) Vytautas Bikauskas
1. [repo](https://gitlab.com/l-lu-u/2024-digital-fabrication/) [www](https://l-lu-u.gitlab.io/2024-digital-fabrication/) Lu Chen
1. [repo](https://gitlab.com/LycheeXixi) [www](https://lycheexixi.gitlab.io/digital-fabrication/) Hongxi Du
1. [repo](https://gitlab.com/grahamfeatherstone/digital-fabrication) [www](https://grahamfeatherstone.gitlab.io/digital-fabrication/) Graham Anthony Featherstone
1. [repo](https://gitlab.com/mikajarvi/digital-fabrication) [www](https://mikajarvi.gitlab.io/digital-fabrication/) Mika Olavi Järvi
1. [repo](https://gitlab.com/miro.keimioniemi/digital-fabrication-portfolio) [www](https://digital-fabrication-portfolio-miro-keimioniemi-a2f2c11a6e705b8f.gitlab.io/) Miro Jesper Keimiöniemi
1. [repo](https://gitlab.com/debkumagai/digital-fabrication) [www](https://debkumagai.gitlab.io/digital-fabrication/) Deborah Naomi Kumagai
1. [repo](https://gitlab.com/Kunnamo/digital-fabrication) [www](https://kunnamo.gitlab.io/digital-fabrication/) Oiva Juhani Kunnamo
1. [repo](https://gitlab.com/liao-project/digital-fabrication) [www](https://liao-project.gitlab.io/digital-fabrication/) Mengzhe Liao
1. [repo](https://gitlab.com/mikko.aleksis.linko/digital-fabrication) [www](https://digital-fabrication-mikko-aleksis-linko-68d09a63e975c53665cdd90.gitlab.io/) Mikko Aleksis Linko
1. [repo](https://gitlab.com/Mengqiao_L/digital-fabrication) [www](https://mengqiao_l.gitlab.io/digital-fabrication/) Mengqiao Liu
1. [repo](https://gitlab.com/yhannahlou1/yh-digital-fabrication/) [www](https://yhannahlou1.gitlab.io/yh-digital-fabrication/) Yihan Lou
1. [repo](https://gitlab.com/aboutRamin/fablab) [www](https://aboutramin.gitlab.io/fablab/) Ramin Mahmoudi
1. [repo](https://gitlab.com/zinaaaa/digital-fabrication) [www](https://zinaaaa.gitlab.io/digital-fabrication/) Zina Marpegan
1. [repo](https://gitlab.com/TomiMonahan/digital-fabrication/) [www](https://tomimonahan.gitlab.io/digital-fabrication/) Tomi Jari Monahan
1. [repo](https://gitlab.com/Jue.Ning/digital-fabrication) [www](https://jue.ning.gitlab.io/digital-fabrication/) Jue Ning
1. [repo](https://gitlab.com/AnttiJNurminen/digital-fabrication) [www](https://anttijnurminen.gitlab.io/digital-fabrication/about.html) Antti Juhani Nurminen
1. [repo](https://gitlab.com/chloepxj/digital-fabrication/) [www](https://chloepxj.gitlab.io/digital-fabrication/) Xiaojie Pi
1. [repo](https://gitlab.com/veikkoraty/digital-fabrication) [www](https://veikkoraty.gitlab.io/digital-fabrication/) Veikko Lauri Iivari Räty
1. [repo](https://gitlab.com/xuefeishi/df1) [www](https://xuefeishi.gitlab.io/df1/) Xuefei Shi
1. [repo](https://gitlab.com/celeste_sanja/digital-fabrication) [www](https://celeste_sanja.gitlab.io/digital-fabrication/) Celeste Sanja Smareglia
1. [repo](https://github.com/teodosin/digital-fabrication) [www](https://teodosin.github.io/digital-fabrication/) Viktor Andrej Teodosin
1. [repo](https://gitlab.com/irmuun-tuguldur/digital-fabrication) [www](https://irmuun-tuguldur.gitlab.io/digital-fabrication/) Irmuun Tuguldur
1. [repo](https://gitlab.com/swirkes/digital-fabrication) [www](https://swirkes.gitlab.io/digital-fabrication/) Shane Thomas Wirkes
1. [repo](https://gitlab.com/thvo-fi/test-portfolio) [www](https://0nitfans.com/) Chi Thanh Vo
1. [repo](https://gitlab.com/zruii/digital-fabrication) [www](https://zruii.gitlab.io/digital-fabrication/) Rui Zeng
