const randomizedStudentList = [];

document.getElementById("randomizer").addEventListener("click", function() {
    loadAndRandomizeStudent();
});

function loadAndRandomizeStudent() {
    // Load the README.md file
    fetch('DF_Studio_STUDENTS.md') // Replace with the path to your README.md
        .then(response => response.text())
        .then(text => {
            const students = extractStudents(text);

            if (students.length == randomizedStudentList.length) {
              alert('Game Over! No more students to randomize...');
              return;
            }

            const randomStudent = students[Math.floor(Math.random() * students.length)];
            displayStudent(randomStudent);
        });
}

function extractStudents(mdText) {
    const md = window.markdownit();
    const renderedHtml = md.render(mdText);
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(renderedHtml, 'text/html');

    const listItems = htmlDoc.querySelectorAll('li');
    return Array.from(listItems).map(li => {
        const repoLinkElement = li.querySelector('a:nth-of-type(1)');
        const wwwLinkElement = li.querySelector('a:nth-of-type(2)');
        if (!repoLinkElement || !wwwLinkElement) {
            return null; // Skip this student if either link is missing
        }

        const repoLink = repoLinkElement.href;
        const wwwLink = wwwLinkElement.href;

        const nameParts = li.textContent.trim().split(/\s+/);
        const name = nameParts.slice(2).join(' ');

        return { repoLink, wwwLink, name };
    }).filter(student => student !== null); // Filter out null entries
}

function displayStudent(student) {
    for (let i = 0; i < randomizedStudentList.length; i++) {
      if (randomizedStudentList[i].name === student.name) {
        loadAndRandomizeStudent();
        return;
      }
    }

    randomizedStudentList.push(student);
    console.log(randomizedStudentList.length + ". Added student", student.name);

    const studentDisplay = document.getElementById("studentDisplay");
    studentDisplay.innerHTML = `
        <p>${student.name}</p>
        <p>[<a href="${student.repoLink}" target="_blank">repo</a>] [<a href="${student.wwwLink}" target="_blank">www</a>]</p>
    `;
}
