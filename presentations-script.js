function loadAndRandomizeStudent() {
  // Load the README.md file
  fetch('DF_Studio_STUDENTS.md') // Replace with the path to your README.md
    .then(response => response.text())
    .then(text => {
      const students = extractStudents(text);

      if (students.length == randomizedStudentList.length) {
        alert('Game Over! No more students to randomize...');
        return;
      }

      const randomStudent = students[Math.floor(Math.random() * students.length)];
      displayStudent(randomStudent);
    });
}

function extractStudents(mdText) {
  const md = window.markdownit();
  const renderedHtml = md.render(mdText);
  const parser = new DOMParser();
  const htmlDoc = parser.parseFromString(renderedHtml, 'text/html');

  const listItems = htmlDoc.querySelectorAll('li');
  return Array.from(listItems).map(li => {
    const repoLinkElement = li.querySelector('a:nth-of-type(1)');
    const wwwLinkElement = li.querySelector('a:nth-of-type(2)');
    if (!repoLinkElement || !wwwLinkElement) {
      return null; // Skip this student if either link is missing
    }

    const repoLink = repoLinkElement.href;
    const wwwLink = wwwLinkElement.href;

    const nameParts = li.textContent.trim().split(/\s+/);
    const name = nameParts.slice(2).join(' ');

    return { repoLink, wwwLink, name };
  }).filter(student => student !== null); // Filter out null entries
}

function displayStudentList(student) {
  for (let i = 0; i < randomizedStudentList.length; i++) {
    if (randomizedStudentList[i].name === student.name) {
    loadAndRandomizeStudent();
    return;
    }
  }

  randomizedStudentList.push(student);
  console.log(randomizedStudentList.length + ". Added student", student.name);

  const studentDisplay = document.getElementById("studentDisplay");
  studentDisplay.innerHTML = `
    <p>${student.name}</p>
    <p>[<a href="${student.repoLink}" target="_blank">repo</a>] [<a href="${student.wwwLink}" target="_blank">www</a>]</p>
  `;
}

async function checkFileExists(url) {
  try {
    const response = await fetch(url, { method: 'HEAD' });
    return response.ok;
  } catch (error) {
    console.error('Error:', error);
    return false;
  }
}

function updateStatus(element, fileExists) {
  if (fileExists) {
    element.classList.add('file-exists');
    element.classList.remove('file-not-exists');
  } else {
    element.classList.add('file-not-exists');
    element.classList.remove('file-exists');
  }
}

fetch('DF_Studio_STUDENTS.md') // Replace with the path to your README.md
  .then(response => response.text())
  .then(text => {
    const students = extractStudents(text);
    const studentListElement = document.getElementById("studentList");
    
    for (const s in students) {
      const student = students[s];
      studentListElement.innerHTML += `
        <tr id="student${s}">
        <td>${student.name}</td>
        <td id="site${s}"><a href="${student.wwwLink}" target="_blank">site</a></td>
        <td id="slide${s}"><a href="${student.wwwLink}presentation.png" target="_blank">slide</a></td>
        <td id="video${s}"><a href="${student.wwwLink}presentation.mp4" target="_blank">video</a></td>
        </tr>
      `;

      // Check site status
      (async () => {
        const url = student.wwwLink + '/index.html';  // Replace with the URL of the file you want to check
        const fileExists = await checkFileExists(url);
        const statusElement = document.getElementById('site' + s);
        updateStatus(statusElement, fileExists);
      })();

      // Check slides
      (async () => {
        const url = student.wwwLink + '/presentation.png';  // Replace with the URL of the file you want to check
        const fileExists = await checkFileExists(url);
        const statusElement = document.getElementById('slide' + s);
        updateStatus(statusElement, fileExists);
      })();

      // Check videos
      (async () => {
        const url = student.wwwLink + '/presentation.mp4';  // Replace with the URL of the file you want to check
        const fileExists = await checkFileExists(url);
        const statusElement = document.getElementById('video' + s);
        updateStatus(statusElement, fileExists);
      })();
    }
  });
